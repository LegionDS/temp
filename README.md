Worklist:
- Make sure no item can purchased from an NPC then sold back to an NPC for **any** profit **at all**
- Make sure no item can be purchased from the Auction House Bot then sold to an NPC for **any** profit **at all**
- Make sure no item can be purchased at an NPC and then sold to the Auction House Bot for **any** profit **at all**  
(_Remember selling an item from an NPC to **other players** via the AH is perfectly normal. We're only concerned with the bot involvement aspect of it_)
* All of the above while accounting for stacks **and** singles at the AH  
(_Remember a small "discount" on the stack is ok, but not anything big, and **must not** cause an NPC price disaster_)
- Note any AH bot pricing that may need adjusting due to an NPC price in a new .txt file